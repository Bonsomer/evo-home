package project.evo.home.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    @FindBy(css = ".menu_lang a")
    private WebElement languageButton;

    @FindBy(css = "a[title='Объявления Электротехника']")
    private WebElement electronics;

    @FindBy(css = "a[title='Искать объявления']")
    private WebElement search;

    @FindBy(css = "a[title='Закладки']")
    private WebElement favorites;

    private WebDriverWait wait;
    private WebDriver driver;

    public MainPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
        driver = webDriver;
        wait = new WebDriverWait(webDriver, 15);
    }

    public void changeLanguageToRussian() {
        wait.until(ExpectedConditions.visibilityOf(languageButton));
        if (languageButton.getAttribute("title").contains("По-русски")) {
            languageButton.click();
        }
    }

    public void openElectronics() {
        wait.until(ExpectedConditions.visibilityOf(electronics));
        electronics.click();
    }

    public void openMenuFavorites() {
        wait.until(ExpectedConditions.visibilityOf(favorites));
        favorites.click();
    }

    public SearchPage openMenuSearch() {
        wait.until(ExpectedConditions.visibilityOf(search));
        search.click();

        return new SearchPage(driver);
    }
}
