package project.evo.home.pageobjects;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComputersPage {
    @FindBy(css = ".msg_column_td a")
    private WebElement priceSort;

    @FindAll({@FindBy(css = ".filter_sel")})
    private List<WebElement> selectTypes;

    @FindBy(css = "#page_main a[href='/ru/electronics/search/']")
    private WebElement extendedSearch;

    @FindBy(css = "table[align='center']")
    private WebElement advertisementsTable;

    @FindBy(id = "a_fav_sel")
    private WebElement addToFavorites;

    @FindBy(id = "filter_frm")
    private WebElement favoritesForm;

    private By advertisementBlock = By.cssSelector("tr[style='cursor: pointer;']");
    private By advertisementTextBlock = By.cssSelector(".d1");

    private WebDriverWait wait;
    private WebDriver driver;

    public ComputersPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
        driver = webDriver;
        wait = new WebDriverWait(webDriver, 15);
    }

    public void addToFavorites() {
        wait.until(ExpectedConditions.visibilityOf(addToFavorites));
        addToFavorites.click();
    }

    public List<String> getAllAdvertisementsTexts() {
        List<String> advertisementsText = new ArrayList<>();
        wait.until(ExpectedConditions.visibilityOf(favoritesForm));
        List<WebElement> advertisements = favoritesForm.findElements(advertisementBlock);
        for (WebElement advertisement : advertisements) {
            advertisementsText.add(advertisement.getAttribute("id"));
        }

        Collections.sort(advertisementsText);

        return advertisementsText;
    }

    public List<String> selectRandomAdvertisements(Integer count) {
        List<String> advertisementsText = new ArrayList<>();
        wait.until(ExpectedConditions.visibilityOf(advertisementsTable));
        List<WebElement> advertisements = advertisementsTable.findElements(advertisementBlock);
        Collections.shuffle(advertisements);

        for (int i = 0; i < count; i++) {
            WebElement element = advertisements.get(i);
            element.findElement(By.cssSelector("input")).click();
            advertisementsText.add(element.getAttribute("id"));
        }

        Collections.sort(advertisementsText);
        return advertisementsText;
    }

    public void selectDeal(String deal) {
        wait.until(ExpectedConditions.visibilityOfAllElements(selectTypes));
        //Worst decision ever. Shit page.
        for (WebElement selectType : selectTypes) {
            if (selectType.getAttribute("class").equals("filter_sel")) {
                Select select = new Select(selectType);
                select.selectByValue(deal);
            }
            break;
        }
    }

    public void sortByPrice() {
        wait.until(ExpectedConditions.visibilityOf(priceSort));
        priceSort.click();
    }

    public SearchPage openExtendedSearch() {
        wait.until(ExpectedConditions.visibilityOf(extendedSearch));
        extendedSearch.click();

        return new SearchPage(driver);
    }

    public void validateAddedToFavoritesAdvertisementsAreEqualToSelected(List<String> added, List<String> selected) {
        Assert.assertEquals(added, selected);
    }
}
