package project.evo.home.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage {
    @FindBy(id = "ptxt")
    private WebElement searchInput;

    @FindBy(css = "input[name='topt[8][min]']")
    private WebElement minPrice;

    @FindBy(css = "input[name='topt[8][max]']")
    private WebElement maxPrice;

    @FindBy(id = "s_region_select")
    private WebElement cities;

    @FindBy(id = "sbtn")
    private WebElement searchButton;

    private WebDriverWait wait;
    private WebDriver driver;

    public SearchPage(WebDriver webDriver) {
        PageFactory.initElements(webDriver, this);
        driver = webDriver;
        wait = new WebDriverWait(webDriver, 15);
    }

    public ComputersPage clickOnSearch() {
        wait.until(ExpectedConditions.visibilityOf(searchButton));
        searchButton.click();

        return new ComputersPage(driver);
    }

    public void selectCity(String city) {
        wait.until(ExpectedConditions.visibilityOf(cities));
        Select select = new Select(cities);
        select.selectByValue(city);
    }

    public void writeToSearch(String input) {
        wait.until(ExpectedConditions.visibilityOf(searchInput));
        clearAndWrite(searchInput, input);
    }

    public void writeToMinPrice(String input) {
        wait.until(ExpectedConditions.visibilityOf(minPrice));
        clearAndWrite(minPrice, input);
    }

    public void writeToMaxPrice(String input) {
        wait.until(ExpectedConditions.visibilityOf(maxPrice));
        clearAndWrite(maxPrice, input);
    }

    private void clearAndWrite(WebElement webElement, String input) {
        webElement.clear();
        webElement.sendKeys(input);
    }
}
