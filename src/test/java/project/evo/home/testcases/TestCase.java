package project.evo.home.testcases;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import project.evo.home.pageobjects.ComputersPage;
import project.evo.home.pageobjects.MainPage;
import project.evo.home.pageobjects.SearchPage;
import project.evo.home.run.InitializeSelenium;

import java.util.List;

@RunWith(BlockJUnit4ClassRunner.class)
public class TestCase extends InitializeSelenium {
    @Test
    public void test() {
        openPage("https://www.ss.com");

        MainPage mainPage = new MainPage(getDriver());
        mainPage.changeLanguageToRussian();
        mainPage.openElectronics();

        SearchPage searchPage = mainPage.openMenuSearch();
        searchPage.writeToSearch("Компьютер");
        searchPage.writeToMinPrice("100");
        searchPage.writeToMaxPrice("1000");
        searchPage.selectCity("riga_f");

        ComputersPage computersPage = searchPage.clickOnSearch();
        computersPage.sortByPrice();
        computersPage.selectDeal("/ru/electronics/search-result/sell/");

        SearchPage extendedSearchPage = computersPage.openExtendedSearch();
        extendedSearchPage.writeToMinPrice("0");
        extendedSearchPage.writeToMaxPrice("300");

        ComputersPage computersPageAfterSearch = extendedSearchPage.clickOnSearch();
        List<String> selectedAdvertisements = computersPageAfterSearch.selectRandomAdvertisements(3);
        computersPageAfterSearch.addToFavorites();

        mainPage.openMenuFavorites();
        List<String> addedAdvertisements = computersPageAfterSearch.getAllAdvertisementsTexts();
        computersPageAfterSearch.validateAddedToFavoritesAdvertisementsAreEqualToSelected(addedAdvertisements, selectedAdvertisements);
    }
}
