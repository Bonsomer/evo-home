package project.evo.home.run;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class InitializeSelenium {
    private WebDriver driver;

    @Before
    public void before() {
        ClassLoader classLoader = getClass().getClassLoader();
        System.setProperty("webdriver.chrome.driver", classLoader.getResource("chromedriver.exe").getPath());

        driver = new ChromeDriver();
        driver.manage().window().fullscreen();
    }

    @After
    public void after() {
        driver.quit();
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void openPage(String url) {
        driver.get(url);
    }
}
